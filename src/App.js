import "./App.css";
import Competence from "./component/Competence/Competence";
import Experience from "./component/Experience/Experience";
import Header from "./component/Header/Header";
import Portofolio from "./component/Portofolio/Portofolio";
import Presentation from "./component/Presentation/Presentation";
import Banner from "./component/Banner/Banner";
import Contact from "./component/Contact/Contact";
import Footer from "./component/Footer/Footer";

function App() {
  return (
    <div className="App">
      <Header />
      <Presentation />
      <Portofolio />
      <Competence />
      <Experience />
      <Banner />
      <Contact />
      <Footer/>
    </div>
  );
}

export default App;
