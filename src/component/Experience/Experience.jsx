import React from "react";
import "./experience.styles.css";

const Experience = () => {
  return (
    <div className="experience">
      <h1>Experiences</h1>
      <div className="grid">
        <div className="exp">
          <h3 className="post">Developpeur Front-End en alternance.</h3>
          <h3 className="year">Janvier 2021</h3>
          <p className="lorem">
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vero
            repellendus natus voluptas adipisci amet fugit labore blanditiis
            pariatur, ea in!
          </p>
        </div>
        <div className="exp">
          <h3 className="post">CS50, Harvard</h3>
          <h3 className="year">Aout-octobre 2020</h3>
          <p className="lorem">
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vero
            repellendus natus voluptas adipisci amet fugit labore blanditiis
            pariatur, ea in!
          </p>
        </div>
        <div className="exp">
          <h3 className="post">Developpeur Junior M2I formation.</h3>
          <h3 className="year">Mars-juillet 2020</h3>
          <p className="lorem">
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vero
            repellendus natus voluptas adipisci amet fugit labore blanditiis
            pariatur, ea in!
          </p>
        </div>
      </div>
    </div>
  );
};

export default Experience;
