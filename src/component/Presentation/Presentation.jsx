import "./presentation.styles.css";

import React from "react";

const Presentation = () => {
  return (
    <div id="presentation">
      <h1>PRESENTATION</h1>
      <div className="container">
      <div className="d1">
        <ol>
          <li>Capacité d'apprentissage</li>
          <li>Collaaboration</li>
          <li>Responsabilité</li>
          <li>Prise de décision</li>
          <li>Gestion du temps</li>
        </ol>
      </div>
      <div className="d2">
        <h1>Triforce !</h1>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat omnis,
        et necessitatibus quia inventore corrupti voluptas, animi eaque, aut
        repudiandae consequuntur? Unde, sequi ipsa soluta accusamus
                  exercitationem impedit nam est?</p>
              <br /><br />
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat omnis,
        et necessitatibus quia inventore corrupti voluptas, animi eaque, aut
        repudiandae consequuntur? Unde, sequi ipsa soluta accusamus
        exercitationem impedit nam est?</p>
      </div>
      </div>
    </div>
  );
};

export default Presentation;
