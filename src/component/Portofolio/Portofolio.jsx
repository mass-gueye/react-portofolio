import React from "react";
import "./portofolio.styles.css";
import img1 from "../../images/1.png";
import img2 from "../../images/2.png";
import img3 from "../../images/3.png";
import img4 from "../../images/4.png";
import img5 from "../../images/5.png";
import img6 from "../../images/6.png";

const Portofolio = () => {
  return (
    <div id="portofolio">
      <h1>PORTOFOLIO</h1>
      <div className="parent">
        <div className="card">
          <div className="card-img">
            <img src={img1} alt="" />
          </div>
          <div className="card-title">Volkeno</div>
          <div className="card-body">
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Odit est
            sint animi fugit optio delectus eaque! Distinctio perferendis
            ducimus.
          </div>
          <div className="card-button">
            <button>Voir le projet github</button>
          </div>
        </div>
        <div className="card">
          <div className="card-img">
            <img src={img2} alt="" />
          </div>
          <div className="card-title">Fewnu</div>
          <div className="card-body">
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Odit est
            sint animi fugit optio delectus eaque! Distinctio perferendis
            ducimus.
          </div>
          <div className="card-button">
            <button>Voir le projet github</button>
          </div>
        </div>
        <div className="card">
          <div className="card-img">
            <img src={img3} alt="" />
          </div>
          <div className="card-title">Fewnu Mobile</div>
          <div className="card-body">
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Odit est
            sint animi fugit optio delectus eaque! Distinctio perferendis
            ducimus.
          </div>
          <div className="card-button">
            <button>Voir le projet github</button>
          </div>
        </div>
        <div className="card">
          <div className="card-img">
            <img src={img4} alt="" />
          </div>
          <div className="card-title">Volkeno</div>
          <div className="card-body">
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Odit est
            sint animi fugit optio delectus eaque! Distinctio perferendis
            ducimus.
          </div>
          <div className="card-button">
            <button>Voir le projet github</button>
          </div>
        </div>
        <div className="card">
          <div className="card-img">
            <img src={img5} alt="" />
          </div>
          <div className="card-title">Bakeli Payement</div>
          <div className="card-body">
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Odit est
            sint animi fugit optio delectus eaque! Distinctio perferendis
            ducimus.
          </div>
          <div className="card-button">
            <button>Voir le projet github</button>
          </div>
        </div>
        <div className="card">
          <div className="card-img">
            <img src={img6} alt="" />
          </div>
          <div className="card-title">Mixte Feewi</div>
          <div className="card-body">
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Odit est
            sint animi fugit optio delectus eaque! Distinctio perferendis
            ducimus.
          </div>
          <div className="card-button">
            <button>Voir le projet github</button>
          </div>
        </div>
        
      </div>
    </div>
  );
};

export default Portofolio;
