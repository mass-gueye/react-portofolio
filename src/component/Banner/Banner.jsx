import React from 'react'
import banner from '../../images/banner.png'
import './banner.styles.css'
const Banner = () => {
    return (
        <div id="banner">
          <img src={banner} alt="" className="banner" />  
        </div>
    )
}

export default Banner
