import "./competence.styles.css";

import React from "react";

const Competence = () => {
  return (
    <div className="competence">
      <h1>Mes Compétences</h1>
      <div className="flex">
        <div className="flex-child">
          <h1 id="outils">OUTILS</h1>
          <br />
          <br />
          <br />
          <br />
          <br />
          <br />
          <div className="outils">
            <ul>
              <li>BOOTSTRAP</li>
              <li>LARAVEL</li>
              <li>REACT</li>
              <li>DJANGO</li>
            </ul>
            <ul>
              <li>
                <progress id="file" value="32" max="100">
                  {" "}
                  32%{" "}
                </progress>
              </li>
              <li>
                <progress id="file" value="32" max="100">
                  {" "}
                  32%{" "}
                </progress>
              </li>
              <li>
                <progress id="file" value="32" max="100">
                  {" "}
                  32%{" "}
                </progress>
              </li>
              <li>
                <progress id="file" value="32" max="100">
                  {" "}
                  32%{" "}
                </progress>
              </li>
            </ul>
          </div>
          <div className="langue">
            <h1>LANGUES</h1>
            <ul>
              <li>FRANCAIS</li>
              <li>ANGLAIS</li>
              <li>PULAAR</li>
            </ul>
          </div>
        </div>
        <div className="flex-child">
          <h1 id="langage">LANGAGES</h1>
          <br />
          <br />
          <br />
          <br />
          <br />
          <br />
          <div className="langage">
            <ul>
              <li>HTML</li>
              <li>CSS</li>
              <li>JAVASCRIPT</li>
              <li>PHP</li>
            </ul>
            <ul>
              <li>
                <progress id="file" value="32" max="100">
                  32%
                </progress>
              </li>
              <li>
                <progress id="file" value="32" max="100">
                  
                  32%
                </progress>
              </li>
              <li>
                <progress id="file" value="32" max="100">
                  32%
                </progress>
              </li>
              <li>
                <progress id="file" value="32" max="100">
                  32%
                </progress>
              </li>
            </ul>
          </div>
          <div className="hobby">
            <h1>HOBBIES</h1>
            <br />
            <ul>
              <li>
               Créateur du blog &#60;&#60;Mon job de dev&#62;&#62;
              </li>
              <li>
                Le developpement Web en general
              </li>
              <li>
                PWA
              </li>
              <li>
                Calcul distribués
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Competence;
