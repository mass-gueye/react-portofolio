import Navbar from "../Navbar/Navbar";
import "./header.styles.css";
import profile from "../../images/president.png";
import wave from "../../images/wave.svg";
import { FaPhone, FaLocationArrow, FaHome } from 'react-icons/fa';
import {HiOutlineMail} from 'react-icons/hi';


const Header = () => {
  return (
    <div className="overlay">
      <div className="bg-mask">
        <Navbar />
        <div className="cnt">
          <div className="pres">
            <h1 className="intro">
              Hello ! J'imite <span>Housseynou Kende</span> Developpeur backend
              senior a Volkeno
            </h1>
            <a href=""><FaPhone className="ri"/>&ensp;&nbsp;77 XXX XX XX</a>
            <a href=""><HiOutlineMail className="ri"/>&ensp;&nbsp;jonh@doe.com</a>
            <a href=""><FaLocationArrow className="ri"/>&ensp;&nbsp;Dakar, Dakar</a>
            <a href=""><FaHome className="ri"/>&ensp;&nbsp;Bakeli</a>
          </div>
          <img src={profile} alt="" className="profile" />
        </div>
        <img src={wave} alt="" className="wave" />
      </div>
    </div>
  );
};

export default Header;
