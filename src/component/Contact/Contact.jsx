import "./contact.styles.css";
import React from "react";
import contactImage from "../../images/prési.png";

const Contact = () => {
  return (
    <div className="contact">
      <h1>RESTONS EN CONTACT</h1>
      <div className="contacts">
        <form action="">
          {/* <label for="fname">Prénom</label> */}
          <input type="text" id="fname" name="firstname" placeholder="Prénom" />
          <br />
          {/* <label for="lname">Nom</label> */}
          <input type="text" id="lname" name="lastname" placeholder="Nom" />
          {/* <label for="subject"></label> */}
          <textarea
            id="subject"
            name="subject"
            placeholder="Votre message"
          ></textarea>

          <input type="submit" value="ENVOYEZ" />
        </form>
        <div className="img-2">
          <img src={contactImage} alt="" />
        </div>
      </div>
    </div>
  );
};

export default Contact;
