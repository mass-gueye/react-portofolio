import { Nav } from "react-bootstrap";
import './navbar.styles.css'
const Navbar = () => {
  return (
    <>
      <nav className="navigation">
        <ul className="nav-items">
          <li className="nav-item"><a href="http://localhost:3000/" className="nav-link">ACCUEIL</a></li>
          <li className="nav-item"><a href="http://localhost:3000/" className="nav-link">PRESENTATION</a></li>
          <li className="nav-item"><a href="http://localhost:3000/" className="nav-link">PORTOFOLIO</a></li>
          <li className="nav-item"><a href="http://localhost:3000/" className="nav-link">COMPETENCES</a></li>
          <li className="nav-item"><a href="http://localhost:3000/" className="nav-link">EXPERIENCES</a></li>
        </ul>
      </nav>
    </>
  );
};

export default Navbar;
